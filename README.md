# How to use GITLAB

1. ssh key 를 등록합니다. <br>
https://www.lainyzine.com/ko/article/creating-ssh-key-for-github/

2. 비밀번호를 등록합니다. 

3. 프로젝트에서 파일을 만들고 작성합니다.

4. 커밋합니다. <br>
$ git init  <br>
$ git add . <br>
$ git commit -m "first commit" <br>
$ git remote add origin https://gitlab.com/hjlee0110.citylabs/firstproject.git <br>
$ git push origin main <br>
    (브랜치를 달리해서 push 한 다음에 merge 해도 됩니다.) <br>

5. 깃랩 래퍼지토리로 와서 잘 올라갔는지 확인합니다.

